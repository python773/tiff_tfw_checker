from tkinter import filedialog
from tkinter import *
import os
import datetime
import sys
from tkinter import ttk
from collections import Counter


print("Program to check file pairs: TIFF and TFW \nCreated by Marek Szczepkowski 02.07.2020")
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)

class Main:
    def __init__(self,master,st=0):
        self.master=master
        master.title('TIFF TFW pair checker')

        #menu
        self.label_1 = Label(master, text="Path: ")
        self.label_1.grid(row=0)
        self.entry_1=Entry(master, width=40)
        self.entry_1.insert(END,'')
        self.entry_1.grid(row=0,column=1)

        self.label_2 = Label(master, text="")
        self.label_2.grid(row=1, column=1)

        self.button_1 = Button(master, text="Get folder path", command=self.main, height=1,width=15)
        self.button_1.grid(row=0,column=3)

        self.close_button = Button(master, text="Exit", command=root.destroy, height=1,width=5)
        self.close_button.grid(row=3,column=4)

        self.info_button = Button(master, text="Info", command=self.info, height=1,width=5)
        self.info_button.grid(row=3,column=0)        

    def info(self):

        self.label_2['text']="Program to check file pairs: TIFF and TFW "

    def pairs(self, path, extInput, expChecked):
        inputFiles=[]
        checkedFiles=[]
        unpaired=[]

        for root, dirs, files in os.walk(path):
            for name in files:
               filename, file_extension = os.path.splitext(name)
               if file_extension in extInput:
                   inputFiles.append(os.path.join(root, name))
               if file_extension in expChecked:
                   checkedFiles.append(os.path.join(root, name))
        
        for i in inputFiles:
            if not i[:i.rfind(".")]+expChecked in checkedFiles:
                unpaired.append(i[:i.rfind(".")]+expChecked)
        
        return unpaired
        
    def main(self):
        
        #get path 
        window=Tk()
        window.withdraw()
        window.title("Sorting images")
        self.label_2['text']="loading files"
        path =  filedialog.askdirectory(parent=window,initialdir=os.getcwd(),title='Please select a directory')
        path=path.replace("/","\\")
        if path=="":
            self.label_2['text']="Select path!"
            return
      
        self.label_2['text']="Working.."
        self.entry_1.delete(0,END)
        self.entry_1.insert(0,path)   
        
 
        result = self.pairs(path, [".tif",".tiff"],".tfw")
        if result:
           with open(path+"\\Missing_files.txt","w") as f:
               f.write("Missing files: " + str(len(result))+"\n")
               for r in result:
                   f.write(f'{r}\n')
    
        self.label_2['text']=f"Done. Find {len(result)} errors.\n Check *.txt file"

        

           
            

        
root=Tk()
bar=Main(root)
root.iconbitmap(resource_path('tiff.ico'))
root.mainloop()





